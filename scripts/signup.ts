﻿/* =============================================================================
  Copyright (C) 2020 - 2021 yumetodo <yume-wikijp@live.jp>
  Distributed under the Boost Software License, Version 1.0.
  (See https://www.boost.org/LICENSE_1_0.txt)
============================================================================= */
import { ready } from './lib/ready';
import { hasProperty } from './lib/hasProperty';
declare const bsCustomFileInput: {
  init(inputSelector?: string, formSelector?: string): void;
  destroy(): void;
};

ready(() => {
  const apiServer = 'http://127.0.0.1:3000';
  const href = window.location.href;
  const assumeInputMatch = (srcElement: HTMLElement, destElement: HTMLElement) => {
    srcElement.addEventListener('input', e => {
      if (!(e.target instanceof HTMLInputElement)) return;
      destElement.setAttribute('pattern', `^${e.target.value}$`);
    });
  };
  const idError = document.getElementById('idError') as HTMLElement;
  const mailAddressError = document.getElementById('mailAddressError') as HTMLElement;
  const icon = document.getElementById('icon') as HTMLInputElement;
  const iconError = document.getElementById('iconError') as HTMLElement;
  const setServerError = (errors: unknown) => {
    if (typeof errors !== 'object' || !hasProperty(errors, 'id', 'mailaddress', 'icon')) return;
    if (typeof errors.id === 'string') {
      idError.innerText = errors.id;
    }
    if (typeof errors.mailaddress === 'string') {
      mailAddressError.innerText = errors.mailaddress;
    }
    if (typeof errors.icon === 'string') {
      iconError.innerText = errors.icon;
    }
  };
  const clearServerError = () => {
    idError.innerHTML = '英数字もしくは<code>_</code>を6文字以上入力してください';
    mailAddressError.innerText = '有効なメールアドレスではありません';
    iconError.innerText = '';
  };
  const postFormData = (postData: string) =>
    fetch(`${apiServer}/v1/producers`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      mode: 'cors',
      body: postData,
    }).then(async r => {
      if (r.status === 201) {
        const url = new URL(href);
        url.pathname = '/confirm_mailaddress.html';
        const param = new URLSearchParams();
        param.set('from_ui', await r.text());
        url.search = `?${param}`;
        window.location.href = url.href;
      } else if (r.status === 404) {
        setServerError(await r.json());
      } else {
        throw new Error(`HTTP Status Code: ${r.status}, responce body: ${await r.text()}`);
      }
    });

  const postIcon = (file: File) =>
    fetch(`${apiServer}/image`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/octet-stream',
      },
      mode: 'cors',
      body: file,
    }).then(async r => {
      if (r.status === 200) {
        return r.text();
      } else if (r.status === 400 || r.status === 401 || r.status === 415) {
        iconError.innerText = await r.text();
        return null;
      } else {
        throw new Error(`HTTP Status Code: ${r.status}, responce body: ${await r.text()}`);
      }
    });

  /**
   * collect all input value except retype field and convert it to JSON
   */
  const createPostData = (form: HTMLFormElement, iconId: string) => {
    const f = Array.from(form);
    const obj = Object.fromEntries(
      f
        .filter((e): e is HTMLInputElement => e instanceof HTMLInputElement && e.id !== 'passretype')
        .map(e => (e.id === 'icon' ? ['icon', iconId] : [e.id, e.value]))
    );
    obj.name += f.filter((e): e is HTMLSelectElement => e instanceof HTMLSelectElement && e.id === 'position')[0].value;
    return JSON.stringify(obj);
  };
  const onSubmit = async (form: HTMLFormElement) => {
    clearServerError();
    if (icon.files == null || icon.files.length !== 1) return;
    return postIcon(icon.files[0]).then(icId => {
      if (icId != null) {
        return postFormData(createPostData(form, icId));
      }
    });
  };

  bsCustomFileInput.init();
  const pass = document.getElementById('pass') as HTMLElement;
  const passretype = document.getElementById('passretype') as HTMLElement;
  assumeInputMatch(pass, passretype);
  clearServerError();
  for (const form of document.forms) {
    form.addEventListener('submit', e => {
      e.preventDefault();
      e.stopPropagation();
      form.classList.add('was-validated');
      if (form.checkValidity()) {
        onSubmit(form).catch(er => console.error(er));
      }
    });
  }
});
