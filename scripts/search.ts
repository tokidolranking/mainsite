import { ready } from './lib/ready';
ready(() => {
  // const apiServer = "http://127.0.0.1:5000";
  // const inputedProducerName = document.getElementById('name').value + document.getElementById('position').value;
  // const inputedProducerId = document.getElementById('id').value;

  // var params = new URLSearchParams({
  // 	producerName: inputedProducerName,
  // 	producerId: inputedProducerId,
  // 	twitter: document.getElementById('twitter').value
  // });
  // var DeleteTarget = [];
  // params.forEach((v, k) => {
  // 	if (v.length === 0) DeleteTarget.push(k);
  // });
  // DeleteTarget.forEach(k => {
  // 	params.delete(k);
  // });

  // var url = new URL(`${apiServer}/producers`);
  // url.search = params.toString();
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  const getFormData = () => {};
  const onSubmit = async () => {
    return getFormData();
  };
  for (const form of document.forms) {
    form.addEventListener('submit', e => {
      e.preventDefault();
      e.stopPropagation();
      form.classList.add('was-validated');
      if (form.checkValidity()) {
        onSubmit().catch(er => console.error(er));
      }
    });
  }
});
