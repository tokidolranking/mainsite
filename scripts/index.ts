/* =============================================================================
  Copyright (C) 2019 - 2021 yumetodo <yume-wikijp@live.jp>
  Distributed under the Boost Software License, Version 1.0.
  (See https://www.boost.org/LICENSE_1_0.txt)
============================================================================= */
import { ready } from './lib/ready';
import { hasProperty } from './lib/hasProperty';

import m from 'mithril';
interface RecordBase {
  music: string;
  key: string;
}
interface Record extends RecordBase {
  picture: string;
  pictauthor?: string;
}
function isRecordBase(r: unknown): r is RecordBase {
  return hasProperty(r, 'music', 'key');
}
function isRecord(r: unknown): r is Record {
  return hasProperty(r, 'music', 'key', 'picture', 'pictauthor');
}
fetch('../res/recordinfo.json')
  .then(r => r.json())
  .then(response => {
    const items = response['record'].map((r: unknown) => {
      if (!isRecordBase(r)) throw new Error('invalid json format');
      return m('section', { class: 'box' }, [
        isRecord(r) ? m('img', { src: `./pic/${r['picture']}` }) : m('div', { class: 'noimage' }),
        m(
          'a',
          {
            href: `https://old.tokidolranking.meigetsu.jp/ranking/total.php?song=${r.key}`,
          },
          m(
            'article',
            { class: 'title' },
            Array.isArray(r.music) ? r.music.map(mu => m('p', mu)) : m('p', r.music)
          )
        ),
      ]);
    });
    ready(() => {
      m.mount(document.getElementById('recordlink') as HTMLElement, {
        view: () => m('div', { class: 'main_container' }, items),
      });
    });
  })
  .catch(reason => {
    ready(() => {
      (document.getElementById('recordlink') as HTMLElement).innerText = reason.toString();
    });
  });
